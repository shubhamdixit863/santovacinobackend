/************************************************************************************
 *                              SACHIN KUMAR
 ***********************************************************************************/

import cookieParser from 'cookie-parser';
import morgan from 'morgan';
import path from 'path';
import helmet from 'helmet';
import cors from 'cors';
import express, { Request, Response, NextFunction } from 'express';
import { BAD_REQUEST } from 'http-status-codes';
import 'express-async-errors';

import BaseRouter from './routes';
import logger from '@shared/Logger';
import { cookieProps } from '@shared/constants';

import mongoose from 'mongoose';
import dbconf from '@config/db'

import { monitor } from '@colyseus/monitor';

import swaggerUi from 'swagger-ui-express';
import swaggerDocument from './swagger/api-doc-v1.json';

// Init express
const app = express();

/************************************************************************************
 *                              Swagger setup
 ***********************************************************************************/


app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));


/************************************************************************************
 *                              Database setup
 ***********************************************************************************/

let dbString = 'mongodb://' + dbconf.dbcredentials.user;
dbString = dbString + ':' + dbconf.dbcredentials.password;
dbString = dbString + '@' + dbconf.dbcredentials.address;
dbString = dbString + ':' + dbconf.dbcredentials.port;
dbString = dbString + '/' + dbconf.dbcredentials.database;

let db="mongodb+srv://logan:1234@cluster0.x6qlj.mongodb.net/santovacino?retryWrites=true&w=majority"
// mongoose.connect(dbString, {
//     useCreateIndex: true,
//     useNewUrlParser: true,
//     useUnifiedTopology: true,
// }, (err) => { if (err) logger.info('DB error: ' + err) });
// mongoose.set('debug', true);
mongoose
 .connect(db, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
 })
 .then((db) => console.log(dbString,"db is connected"))
 .catch((err) => console.log(err));
mongoose.set('debug', true);


/************************************************************************************
 *                              Set basic express settings
 ***********************************************************************************/
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser(process.env.COOKIE_SECRET));



// Show routes called in console during development
if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}

// Security
if (process.env.NODE_ENV === 'production') {
    app.use(helmet());
}

/************************************************************************************
 *                              Routes
 ***********************************************************************************/
app.use('/colyseus', monitor());
app.use('/api', BaseRouter);

// Print API errors
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
    logger.error(err.message, err);
    return res.status(BAD_REQUEST).json({
        error: err.message,
    });
});



/************************************************************************************
 *                              Serve front-end content
 ***********************************************************************************/

const viewsDir = path.join(__dirname, 'views');
app.set('views', viewsDir);
const staticDir = path.join(__dirname, 'public');
app.use(express.static(staticDir));

app.get('/', (req: Request, res: Response) => {
    res.sendFile('login.html', { root: viewsDir });
});

app.get('/users', (req: Request, res: Response) => {
    const jwt = req.signedCookies[cookieProps.key];
    if (!jwt) {
        res.redirect('/');
    } else {
        res.sendFile('users.html', { root: viewsDir });
    }
});


// Export express instance
export default app;
