
import './LoadEnv'; // Must be the first import

import app from '@server';
import logger from '@shared/Logger';
import { createServer } from 'http';
import { RedisPresence, Server } from 'colyseus';
import { SampleTestRoom } from '@shared/SampleTestRoom';
import { CustomLobbyRoom } from '@shared/LobbyRoom';

const port = Number(process.env.PORT || 3000);

/************************************************************************************
 *                              Colyseus setup
 ***********************************************************************************/
const gameServer = new Server({
    verifyClient: (info, next) => {
        // logger.info('verifyClient', info)
        next(true)
    },
    server: createServer(app),
    // presence: new RedisPresence({
    //     host: process.env.REDIS_HOST,
    //     port: Number(process.env.REDIS_PORT)
    // })
});

// Room handler
gameServer.define('gameRoom', SampleTestRoom).enableRealtimeListing()
gameServer.define('lobbyRoom', CustomLobbyRoom);
// Start the server
gameServer.listen(port);

logger.info('Express server started on port: ' + port)

export default gameServer;