import * as constantsKeys from '@shared/constantsKeys';
import dotenv from 'dotenv';
dotenv.config();

console.log(process.env.DB_HOST)

function getConfig() {
    const configObj = {
        [constantsKeys.KEY_LABEL]: 'Santo-Vecino',
        [constantsKeys.KEY_DBCREDENTIALS]: {
            [constantsKeys.KEY_USER]: process.env.DB_USER,
            [constantsKeys.KEY_PASSWORD]: process.env.DB_PASSWORD,
            [constantsKeys.KEY_DATABASE]: process.env.DB_NAME,
            [constantsKeys.KEY_ADDRESS]: process.env.DB_HOST,
            [constantsKeys.KEY_BEFORE_IP_ADDRESS]: 'http://', 
            [constantsKeys.KEY_PORT]: process.env.DB_PORT
        }   
    }
    if (process.env.NODE_ENV === 'production') {
        configObj[constantsKeys.KEY_DBCREDENTIALS][constantsKeys.KEY_USER] = process.env.DB_USER;
        configObj[constantsKeys.KEY_DBCREDENTIALS][constantsKeys.KEY_PASSWORD] = process.env.DB_PASSWORD;
        configObj[constantsKeys.KEY_DBCREDENTIALS][constantsKeys.KEY_ADDRESS] = process.env.DB_HOST;
        configObj[constantsKeys.KEY_DBCREDENTIALS][constantsKeys.KEY_DATABASE] = process.env.DB_NAME;
    }
    return configObj;
       
}

const dbconf = getConfig();
export default dbconf;