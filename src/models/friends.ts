import { Gender, ROLES, Platform, Status, LOGIN_STATUS } from '@shared/constants';
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const friends = new Schema({
  sender_id: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  receiver_id: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  status: { type: Number, default: Status.inactive },
  isBlocked: { type: Number, default: Status.inactive },
  isSend: { type: Boolean, default: true },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  isDeleted: { type: Boolean, default: false },
})


export default mongoose.model('friends', friends)
