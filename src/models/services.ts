import { Gender, ROLES, Platform, Status, LOGIN_STATUS } from '@shared/constants';
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const service = new Schema({
    userid: Schema.Types.ObjectId,
    name: String,
    image: String,
    description : String,
    service_type : String,
    cate_id : Schema.Types.ObjectId,
    status: { type: Number, default: Status.active },
    isDeleted: { type: Boolean, default: false },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
})


export default mongoose.model('service', service)
