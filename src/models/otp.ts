import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const otp = new Schema({
    user_id: Schema.Types.ObjectId,
    country_code: Number,
    phone: Number,
    role: String,
    otp: String,
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    isDeleted: { type: Boolean, default: false },
})


export default mongoose.model('otp', otp)
