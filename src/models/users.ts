import { Gender, ROLES, Platform, PERMISSIONS,REGISTERTYPE, Status, LOGIN_STATUS } from '@shared/constants';
import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const users = new Schema({
    isAnonymous: Boolean,
    email: { type: String, lowercase: true, trim: true, required: true, unique: true },
    password: String,
    passwordSalt: String,
    gender: { type: Number, required: true, default:Gender.male },
    role: { type: String, default: ROLES.USER },
    first_name: String,
    last_name: String,
    profile_picture: String,
    register_type : { type: Number, default:REGISTERTYPE.SELFREGISTER },
    token:String,
    mobile_number: { type: String },
    address: { type: String },
    lat: { type: Number },
    long: { type: Number },
    lang: { type: String, default: 'en' },
    timezone: String,
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    status: { type: Status, default: Status.active },
    isDeleted: { type: Boolean, default: false },
    deleteOn: Date
})



export default mongoose.model('users', users)