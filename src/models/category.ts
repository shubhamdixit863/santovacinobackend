import { Gender, ROLES, Platform, Status, LOGIN_STATUS } from '@shared/constants';
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const category = new Schema({
    userid: Schema.Types.ObjectId,
    label: String,
    image: String,
    status: { type: Number, default: Status.active },
    isDeleted: { type: Boolean, default: false },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
})


export default mongoose.model('category', category)
