import { Gender, ROLES, Platform, Status, LOGIN_STATUS } from '@shared/constants';
import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const myoffers = new Schema({

    login_userId : {
        type: Schema.Types.ObjectId,
        ref: 'users'
      },
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'users'
      },
    product_id : {
        type: Schema.Types.ObjectId,
        ref: 'product' 
    }, 
    service_id : {
        type: Schema.Types.ObjectId,
        ref: 'service',
    },
    isDeleted: { type: Boolean, default: false },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },

})


export default mongoose.model('myoffers', myoffers)
