import { Gender, ROLES, Platform, Status, LOGIN_STATUS } from '@shared/constants';
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const notifications = new Schema({
  sender_id: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  receiver_id: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  notification_message: String,
  notification_type: String,
  status: { type: Number, default: Status.active },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  isDeleted: { type: Boolean, default: false },
})


export default mongoose.model('notifications', notifications)
