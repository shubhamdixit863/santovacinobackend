import { Gender, ROLES, Platform, Status, LOGIN_STATUS } from '@shared/constants';
import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const assets = new Schema({

    file_url: { type: String },
    version: { type: String, default: null },
    status: { type: Number, default: Status.active },
    isDeleted: { type: Boolean, default: false },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },

})


export default mongoose.model('assets', assets)
