import users from './users'
import friends from './friends'
import assets from './assets'
import otp from './otp'
import notification from './notification'
import category from './category'
import product from './product'
import service from './services'
import offer from './offers'


export { users as usersModel }
export { friends as friendsModel }
export { assets as assetsModel }
export { otp as otpModel }
export { notification as notificationModel }
export { category as categoryModel }
export { product as productModel }
export { service as serviceModel }
export { offer as offersModel }




