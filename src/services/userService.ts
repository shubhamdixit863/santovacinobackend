import { usersModel } from '@models/index'
import _default from '@colyseus/social/src/models/FriendRequest'
import mongoose from 'mongoose';


export const findUserByEmail = async (emailId: string) => {
    return await usersModel.findOne({ 'email': emailId, isDeleted: false })
}

export const findByDisplayName = async (displyName: string) => {
    return await usersModel.findOne({ 'displayName': displyName, isDeleted: false })
}

export const updateById = async (_id: Object, data: any) => {
    return await usersModel.updateOne({ _id }, data,{new:true})
}

export const findUserById = async (_id: string) => {
    return await usersModel.findOne({ _id, isDeleted: false })
}

export const findFriendByUserId = async (_id: Object) => {
    return await usersModel.findOne({ 'friendIds.user_id':{ $in: _id } })
}
