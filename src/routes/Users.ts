import { hashPassword, isValidPassword } from '@colyseus/social/src/auth';
import * as db from '@models/index';
import * as User from '@services/userService';
import { BadRequestResponse, SomethingWentWrong, SuccessResponse, passwordUpdate, match_not_password, Status, ROLES } from '@shared/constants';
import { MWorngPassword,MSuccess,MPasswordNotMatched, MINVALIDTOKEN, MPROFILEUPLOAD } from '@shared/Message';
import { isPasswordValid, genPassword } from '@shared/functions';
import { JwtService } from '@shared/JwtService';
import { SREGISTER, SSOCIALREGISTER, SCHANGEPASSWORD, SEDITPROFILE, SSETPASSWORD, SPROFILE_DETAILS } from '@shared/Slug';
import { VLogin, VRegisterUser, VProfile } from '@shared/validations';
import { NextFunction, Request, Response, Router } from 'express';
import { validationResult } from 'express-validator';
import { BAD_REQUEST, CREATED, INTERNAL_SERVER_ERROR, OK, UNAUTHORIZED, NOT_FOUND } from 'http-status-codes';
import multer from 'multer';
import AWS from'aws-sdk';
import { v4 as uuidv4 } from'uuid';



// Init shared
const router = Router() // .use(adminMW);
const jwtService = new JwtService();

const s3 = new AWS.S3({
    accessKeyId: "AKIASOELPY27HZFM55EF",
    secretAccessKey: "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"
})

const storage = multer.memoryStorage()
const upload = multer({storage}).single('file')


/******************************************************************************
 *                       Register - "POST /api/users/register"
 ******************************************************************************/

const userRegister = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { email, role, first_name, last_name, mobile_number, address } = req.body
    db.usersModel.create({ role, email, first_name, last_name, mobile_number, address }).then(async (user: any) => {
        user = user.toObject()
        user.token = await jwtService.getJwt({ _id: user._id, email: user.email, mobile_number: user.mobile_number, role: user.role })
        await User.updateById(user._id, { token: user.token })
        SuccessResponse.data = user
        res.status(CREATED).json(SuccessResponse).end()
    }).catch(err => {
        SomethingWentWrong.data = err
        res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
    })
}

const setPassword = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    const { newPassword, confirmPassword } = req.body
    User.findUserById(_id).then(async (userDetails: any) => {
        if (userDetails) {
            User.findUserByEmail(userDetails.email).then(async (user: any) => {
                if (newPassword == confirmPassword) {
                    const passwordSaltHash = hashPassword(req.body.newPassword);
                    const password = passwordSaltHash.hash;
                    const passwordSalt = passwordSaltHash.salt;
                    db.usersModel.updateOne({ _id: loginuser }, { $set: { password: password, passwordSalt: passwordSalt } }).then((succ: any) => {
                        res.status(OK).json(SuccessResponse).end()
                    })
                }
                else {
                    match_not_password.message = MPasswordNotMatched
                    return res.status(NOT_FOUND).json(match_not_password).end();
                }
            })
        }
    })
}

const changePassword = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    const { oldPassword, newPassword, confirmPassword } = req.body
    User.findUserById(_id).then(async (userDetails: any) => {
        if (userDetails) {
            User.findUserByEmail(userDetails.email).then(async (user: any) => {
                if (user && isPasswordValid(user, oldPassword)) {
                    if (newPassword == confirmPassword) {
                        const passwordSaltHash = hashPassword(req.body.newPassword);
                        const password = passwordSaltHash.hash;
                        const passwordSalt = passwordSaltHash.salt;
                        db.usersModel.updateOne({ _id: loginuser }, { $set: { password: password, passwordSalt: passwordSalt } }).then((succ: any) => {
                            res.status(OK).json(SuccessResponse).end()
                        })
                    }
                    else {
                        match_not_password.message = MPasswordNotMatched
                        return res.status(NOT_FOUND).json(match_not_password).end();
                    }
                }
                else {
                    passwordUpdate.message = MWorngPassword
                    return res.status(NOT_FOUND).json(passwordUpdate).end();
                }
            })
        }
    })

}
const social_register = async (req: Request, res: Response) => {

    
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    

    const { email, role, first_name, register_type, last_name, mobile_number, address } = req.body;
    db.usersModel.findOneAndUpdate({email:email},{$set:{email, role, first_name, register_type, last_name, mobile_number, address}},{ upsert: true, new: true, setDefaultsOnInsert: true }).then(async (user: any) => {
        user = user.toObject()
        user.token = await jwtService.getJwt({ _id: user._id, email: user.email, mobile_number: user.mobile_number, role: user.role })
        await User.updateById(user._id, { token: user.token })
        SuccessResponse.data = user
        res.status(CREATED).json(SuccessResponse).end()
    }).catch(err => {
        console.log(err);
        SomethingWentWrong.data = err
        res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
    })

/*
            db.usersModel.create({ role, email, first_name, register_type, last_name, mobile_number, address }).then(async (user: any) => {
                user = user.toObject()
                user.token = await jwtService.getJwt({ _id: user._id, email: user.email, mobile_number: user.mobile_number, role: user.role })
                await User.updateById(user._id, { token: user.token })
                SuccessResponse.data = user
                res.status(CREATED).json(SuccessResponse).end()
            }).catch(err => {
                console.log(err);
                SomethingWentWrong.data = err
                res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
            })
            */

        
       
       
        }


  


const edit_upload_profile = async (req: Request, res: Response) => {
    //const errors = validationResult(req); // validation
    /*
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    */
   console.log(req.file);


    let myFile = req.file.originalname.split(".")
    console.log(myFile);
    const fileType = myFile[myFile.length - 1]

    /*
    Put this code inside condition
    s3.createBucket({
        Bucket : 'santovacino',
      },(error:any, data:any)=>{
          console.log(data);

      })
      */
    const params = {
        Bucket: "santovacinobucket",
        Key: `${uuidv4()}.${fileType}`,
        Body: req.file.buffer
    }
    s3.upload(params, async(error:any, data:any) => {
        if(error){
            console.log(error);
            res.status(500).send(error)
        }

        else{
            const profile_picture  = data.Location;
            console.log(profile_picture);
            console.log("heyyyy----",req.headers.authorization);
            try {

               const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
            
                let loginuser = _id;
                db.usersModel.findOne({ _id: loginuser }).then((user: any) => {
                    if ( user && user.role == ROLES.USER) {
                        db.usersModel.updateOne({ _id: loginuser }, { $set: { profile_picture: profile_picture } }).then((succ: any) => {
                            SuccessResponse.message = MPROFILEUPLOAD;
                            SuccessResponse.profileUrl=profile_picture;
                            res.status(OK).json(SuccessResponse).end()
                        })
                    } else {
                        BadRequestResponse.message = MINVALIDTOKEN
                        return res.status(NOT_FOUND).json(BadRequestResponse).end();
                    }
                })
                
            } catch (error) {
                console.log("error------",error);
            }
   

        }
    })

    
}

const UserProfileDetails = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    db.usersModel.findOne({ _id: loginuser }).then((user: any) => {
        if (user && user.role == ROLES.USER) {
            SuccessResponse.message = MSuccess
            SuccessResponse.data = user
            res.status(OK).json(SuccessResponse).end()
        } else {
            BadRequestResponse.message = MINVALIDTOKEN
            return res.status(NOT_FOUND).json(BadRequestResponse).end();
        }
    })
}




router.post(SREGISTER, VRegisterUser, userRegister);
router.post(SSOCIALREGISTER, social_register);
router.post(SSETPASSWORD, setPassword);
router.post(SCHANGEPASSWORD, changePassword);
router.post(SEDITPROFILE,upload, edit_upload_profile);
router.get(SPROFILE_DETAILS, UserProfileDetails);



/******************************************************************************
 *                       Update - "PUT /api/users/update"
 ******************************************************************************/

router.put('/update', async (req: Request, res: Response) => { });


/******************************************************************************
 *                    Delete - "DELETE /api/users/delete/:id"
 ******************************************************************************/

router.delete('/delete/:id', async (req: Request, res: Response) => { });


/******************************************************************************
 *                                     Export
 ******************************************************************************/

export default router;
