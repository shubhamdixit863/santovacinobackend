import { BadRequestResponse, SuccessResponse } from '@shared/constants';
import { Request, Response, Router } from 'express';
import { restUserVerify } from './middleware';
import {SGET_ASSETS } from '@shared/Slug';
import { MAlreadyExistInFriendList, MSomethingWentWrong, MNoFriendsExist, MSuccess } from '@shared/Message';
import * as db from '@models/index'
import { JwtService } from '@shared/JwtService';
import { validationResult, Result } from 'express-validator';
import mongoose from 'mongoose';
import { assetsModel } from '@models/index';
import { Gender, ROLES, Platform, Status, SomethingWentWrong, LOGIN_STATUS } from '@shared/constants';
import { BAD_REQUEST, CREATED, INTERNAL_SERVER_ERROR, NOT_FOUND, OK, UNAUTHORIZED } from 'http-status-codes';



// Init shared
const router = Router().use(restUserVerify);

const jwtService = new JwtService();


/******************************************************************************
 *                       Register - "POST /api/assets/getAssets"
 ******************************************************************************/

const getAssetsList = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    assetsModel.find({}).then(async assets => {
        if(assets){
            SuccessResponse.data = assets
            SuccessResponse.message = MSuccess
            res.status(OK).json(SuccessResponse).end()
        }
    })
}


router.get(SGET_ASSETS, getAssetsList);

export default router
