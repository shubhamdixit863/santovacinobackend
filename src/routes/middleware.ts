import { cookieProps, UnauthorizedResponse } from '@shared/constants';
import { JwtService } from '@shared/JwtService';
import logger from '@shared/Logger';
import { NextFunction, Request, Response } from 'express';
import { UNAUTHORIZED } from 'http-status-codes';
import { MUnauth } from '@shared/Message';


const jwtService = new JwtService();

// THIS FUNCTION IS NOT IN USE
// Middleware to verify if user is an admin
export const adminMW = async (req: Request, res: Response, next: NextFunction) => {
  
    try {
        // Get json-web-token
        const jwt = req.headers.authorization;// req.signedCookies[cookieProps.key];
        if (!jwt) {
            res.status(UNAUTHORIZED).json(UnauthorizedResponse).end();
        } else {
            const clientData = await jwtService.decodeJwt(jwt);
            return clientData;
        }

    } catch (err) {
        return res.status(UNAUTHORIZED).json(UnauthorizedResponse).end();
    }
};

export const restUserVerify = async (req: Request, res: Response, next: NextFunction) => {
    const jwt = req.headers.authorization;
    
    try {
        if (!jwt) {
            throw Error(MUnauth);
        } else {
            const clientData = await jwtService.decodeJwt(jwt);
           // console.log('---------->', clientData)
            if (clientData) {
                next()
            } else {
                throw Error(MUnauth)
            }
        }
    } catch (err) {
        UnauthorizedResponse.message = err.message
        return res.status(UNAUTHORIZED).json(UnauthorizedResponse);
    }
}



export const wsUserVerify = async (token: string) => {
    try { 
        if (!token) {
            throw Error('JWT not present in signed cookie.');
        } else {
            const decoded = await jwtService.decodeJwt(token)
            logger.info(decoded)
            return decoded;
        }
    } catch (err) {
        return false;
    }
}