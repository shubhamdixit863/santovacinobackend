import { Router } from 'express';
import UserRouter from './Users';
import AuthRouter from './Auth';
import RoomRouter from './Rooms';
import FriendRouter from './Friends';
import AssetsRouter from './Assets';
import CategoryRouter from './Category';
import ProductRouter from './Product';
import ServiceRouter from './Services';
import OfferRouter from './Offers';




import { SROOM, SUSERS, SAUTH, SFRIEND,SASSETS,SCATEGORY,SPRODUCT,SSERVICE,SOFFER } from '@shared/Slug';


// Init router and path
const router = Router();

// Add sub-routes
router.use(SUSERS, UserRouter);
router.use(SAUTH, AuthRouter);
router.use(SROOM, RoomRouter);
router.use(SFRIEND, FriendRouter);
router.use(SASSETS, AssetsRouter);
router.use(SCATEGORY, CategoryRouter);
router.use(SPRODUCT, ProductRouter);
router.use(SSERVICE, ServiceRouter);
router.use(SOFFER, OfferRouter);






// Export the base-router
export default router;
