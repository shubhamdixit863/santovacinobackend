import { BadRequestResponse, SuccessResponse } from '@shared/constants';
import { SSEND_FRIEND_REQUEST, SGET_FRIENDS_LIST, SPENDING_FRIENDS_LIST, SBLOCKED_FRIENDS_LIST, SACCEPT_FRIEND_REQUEST, SREMOVE_FRIEND, SBLOCK_FRIEND, SUNBLOCK_FRIEND, SFILE } from '@shared/Slug';
import { VSendFriendRequest, VAcceptFriendRequest } from '@shared/validations';
import { Request, Response, Router } from 'express';
import { validationResult, Result } from 'express-validator';
import { restUserVerify } from './middleware';
import * as User from '@services/userService';
import { MAlreadyExistInFriendList, MSomethingWentWrong, MNoFriendsExist, MSuccess } from '@shared/Message';
import * as db from '@models/index'
import { JwtService } from '@shared/JwtService';
import mongoose from 'mongoose';
import { friendsModel } from '@models/index';
import { assetsModel } from '@models/index';
import { Gender, ROLES, Platform, Status, SomethingWentWrong, LOGIN_STATUS } from '@shared/constants';
import { BAD_REQUEST, CREATED, INTERNAL_SERVER_ERROR, NOT_FOUND, OK, UNAUTHORIZED } from 'http-status-codes';
import { hashPassword } from '@colyseus/social/src/auth';
import { upload } from '@shared/functions'
import { file } from 'find';
const singleupload = upload.single('photo')



// Init shared
const router = Router().use(restUserVerify);

const jwtService = new JwtService();

/******************************************************************************
 *                       Register - "POST /api/friend/sendRequest"
 ******************************************************************************/
const sendFriendRequest = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { user_id } = req.body
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;

    var newFriends = new friendsModel({
        sender_id: loginuser,
        receiver_id: user_id,
    });
    friendsModel.findOne({ receiver_id: user_id, sender_id: loginuser,isSend:true }).then(async friend => {
        if (friend) {
            BadRequestResponse.message = MAlreadyExistInFriendList
            return res.status(BAD_REQUEST).json(BadRequestResponse).end();
        } else {
            newFriends.save(function (err, saveData) {
                if (err) {
                    SomethingWentWrong.data = err
                    res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
                } else {
                    // SuccessResponse.data = saveData
                    SuccessResponse.message = MSuccess
                    res.status(OK).json(SuccessResponse).end()
                }
            });
        }
    })
}

const getFriendsList = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    friendsModel.find({
        receiver_id: mongoose.Types.ObjectId(loginuser), status: Status.active,
        isBlocked: Status.inactive,isSend:true
    }).populate('sender_id', '_id displayName roleId primaryMemberID email dob').exec(function (err, friends) {
        console.log("=========>", friends)
        if (err) throw err;
        if (friends) {
            let resultSet = friends.map(function (friend: any) {
                let obj = {
                    _id: '',
                    displayName: '',
                    roleId: '',
                    primaryMemberID: '',
                    email: '',
                    dob: ''
                };

                if (friend.sender_id) {
                    obj._id = friend.sender_id._id;
                    obj.displayName = friend.sender_id.displayName;
                    obj.roleId = friend.sender_id.roleId;
                    obj.primaryMemberID = friend.sender_id.primaryMemberID;
                    obj.email = friend.sender_id.email;
                    obj.dob = friend.sender_id.dob;
                    return obj;
                }
            });
            Promise.all(resultSet)
                .then(function () {
                    SuccessResponse.data = resultSet
                    SuccessResponse.message = MSuccess
                    res.status(OK).json(SuccessResponse).end()
                });
        } else {
            BadRequestResponse.message = MNoFriendsExist
            return res.status(NOT_FOUND).json(BadRequestResponse).end();
        }

    })
}


const acceptFriendRequest = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { user_id } = req.body
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    await friendsModel.updateOne({ receiver_id: loginuser, sender_id: user_id,isSend:true },
        { $set: { status: Status.active } }).then((succ: any) => {
            SuccessResponse.message = MSuccess
            res.status(OK).json(SuccessResponse).end()
        }).catch(err => {
            console.log("errrrrrrrrr", err)
            SomethingWentWrong.data = err
            res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
        })

}

const removeFriend = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { user_id } = req.body
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    await friendsModel.updateOne({ receiver_id: user_id, sender_id: loginuser,isSend:true },
        { $set: { status: Status.inactive,isSend:false } }).then((succ: any) => {
            SuccessResponse.message = MSuccess
            res.status(OK).json(SuccessResponse).end()
        }).catch(err => {
            SomethingWentWrong.data = err
            res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
        })

}

const blockFriend = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { user_id } = req.body
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    await friendsModel.updateOne({ receiver_id: loginuser, sender_id: user_id,isSend:true },
        { $set: { isBlocked: Status.active } }).then((succ: any) => {
            SuccessResponse.message = MSuccess
            res.status(OK).json(SuccessResponse).end()
        }).catch(err => {
            SomethingWentWrong.data = err
            res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
        })

}
const unBlockFriend = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { user_id } = req.body
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    await friendsModel.updateOne({ receiver_id: loginuser, sender_id: user_id,isSend:true },
        { $set: { isBlocked: Status.inactive, status: Status.active } }).then((succ: any) => {
            SuccessResponse.message = MSuccess
            res.status(OK).json(SuccessResponse).end()
        }).catch(err => {
            SomethingWentWrong.data = err
            res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
        })

}


const getPendingFriendsRequestList = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    friendsModel.find({
        receiver_id: mongoose.Types.ObjectId(loginuser), status: Status.inactive,
        isBlocked: Status.inactive,isSend:true
    }).populate('sender_id', '_id displayName roleId primaryMemberID email dob').exec(function (err, friends) {
        if (err) throw err;
        if (friends.length != 0) {
            let resultSet = friends.map(function (friend: any) {
                let obj = {
                    _id: '',
                    displayName: '',
                    roleId: '',
                    primaryMemberID: '',
                    email: '',
                    dob: ''
                };
                if (friend.sender_id) {
                    obj._id = friend.sender_id._id;
                    obj.displayName = friend.sender_id.displayName;
                    obj.roleId = friend.sender_id.roleId;
                    obj.primaryMemberID = friend.sender_id.primaryMemberID;
                    obj.email = friend.sender_id.email;
                    obj.dob = friend.sender_id.dob;
                    return obj;
                }
            });
            Promise.all(resultSet)
                .then(function () {
                    SuccessResponse.data = resultSet
                    SuccessResponse.message = MSuccess
                    res.status(OK).json(SuccessResponse).end()
                });
        } else {
            BadRequestResponse.message = MNoFriendsExist
            return res.status(NOT_FOUND).json(BadRequestResponse).end();
        }

    })
}
const getBlockedFriendsList = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    friendsModel.find({
        receiver_id: mongoose.Types.ObjectId(loginuser),
        isBlocked: Status.active,isSend:true
    }).populate('sender_id', '_id displayName roleId primaryMemberID email dob').exec(function (err, friends) {
        if (err) throw err;
        if (friends.length != 0) {
            let resultSet = friends.map(function (friend: any) {
                let obj = {
                    _id: '',
                    displayName: '',
                    roleId: '',
                    primaryMemberID: '',
                    email: '',
                    dob: ''
                };
                if (friend.sender_id) {
                    obj._id = friend.sender_id._id;
                    obj.displayName = friend.sender_id.displayName;
                    obj.roleId = friend.sender_id.roleId;
                    obj.primaryMemberID = friend.sender_id.primaryMemberID;
                    obj.email = friend.sender_id.email;
                    obj.dob = friend.sender_id.dob;
                    return obj;
                }
            });
            Promise.all(resultSet)
                .then(function () {
                    SuccessResponse.data = resultSet
                    SuccessResponse.message = MSuccess
                    res.status(OK).json(SuccessResponse).end()
                });
        } else {
            BadRequestResponse.message = MNoFriendsExist
            return res.status(NOT_FOUND).json(BadRequestResponse).end();
        }

    })
}


router.post('/file-upload', singleupload, (req: any, res: any) => {
    var nameOfFile =req.file.originalname;
    var regex   = /[+-]?\d+(\.\d+)?/g;
    var str     = nameOfFile;
    var floats = str.match(regex).map(function(v:any) { return (v); });
    var version = floats.join('.');
    var newAssets = new assetsModel({
        file_url:req.file.location,
        version:version,
    });
    newAssets.save(function (err,saveData) {
        if (err) {
            SomethingWentWrong.data = err
            res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
        } else {
            SuccessResponse.message = MSuccess
            res.status(OK).json(SuccessResponse).end()
        }
    });

})

router.post(SSEND_FRIEND_REQUEST, VSendFriendRequest, sendFriendRequest);
router.get(SGET_FRIENDS_LIST, getFriendsList);
router.post(SACCEPT_FRIEND_REQUEST, VAcceptFriendRequest, acceptFriendRequest);
router.post(SREMOVE_FRIEND, VAcceptFriendRequest, removeFriend);
router.post(SBLOCK_FRIEND, VAcceptFriendRequest, blockFriend);
router.post(SUNBLOCK_FRIEND, VAcceptFriendRequest, unBlockFriend);
router.get(SPENDING_FRIENDS_LIST, getPendingFriendsRequestList);
router.get(SBLOCKED_FRIENDS_LIST, getBlockedFriendsList);











export default router