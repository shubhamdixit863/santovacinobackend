import * as db from '@models/index';
import { BadRequestResponse, SomethingWentWrong, SuccessResponse } from '@shared/constants';
import { MWorngPassword,MSuccess,MNameAlreadyAdded,MProductSUpdated } from '@shared/Message';
import { JwtService } from '@shared/JwtService';
import { SADD_PRODUCT, SGET_PRODUCT, SGET_PRODUCT_BYNAME,SEDIT_PRODUCT} from '@shared/Slug';
import { VProfile } from '@shared/validations';
import { NextFunction, Request, Response, Router } from 'express';
import { validationResult } from 'express-validator';
import { BAD_REQUEST, CREATED, INTERNAL_SERVER_ERROR, OK, UNAUTHORIZED, NOT_FOUND } from 'http-status-codes';
import category from '@models/category';
import { mongoose } from '@colyseus/social';
import product from '@models/product';




// Init shared
const router = Router() // .use(adminMW);
const jwtService = new JwtService();


/******************************************************************************
 *                       Register - "POST /api/product/addProduct"
 ******************************************************************************/

const addProduct = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    const name = req.body.name;
    const image = req.body.image;
    const description = req.body.description;
    const product_type = req.body.product_type;
    const cate_id = req.body.cate_id;
    var newProduct = new db.productModel({
        name: name,
        image: image,
        description: description,
        product_type : product_type,
        cate_id : cate_id,
        userid: loginuser
    });
    newProduct.save().then(async (product: any) => {
        product = product.toObject()
        SuccessResponse.message = MSuccess
        SuccessResponse.data = product
        res.status(CREATED).json(SuccessResponse).end()
    }).catch(err => {
        SomethingWentWrong.data = err
        res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
    })
}

const editProduct = async (req:Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    const loginuser = _id;
    const id  = req.body._id;
    const name = req.body.name;
    const image = req.body.image;
    const description = req.body.description;
    const product_type = req.body.product_type;
    const cate_id = req.body.cate_id;
    db.productModel.findOne({name:{'$regex' : name, '$options' : 'i'},_id: {'$ne':id}}).then(async (product:any) => {
    if(product){
        BadRequestResponse.message = MNameAlreadyAdded
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    } else {
      let product = {
        name:name,
        image:image,
        description:description,
        product_type:product_type,
        cate_id:cate_id,
      }
    db.productModel.updateOne({_id:id}, {$set:product},{new:true}).then(async (productData) => {
        SuccessResponse.data = productData
        SuccessResponse.message = MProductSUpdated
        res.status(OK).json(SuccessResponse).end()  
    }).catch(err => {
        SomethingWentWrong.data = err
        res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
    })  
    }
    })
}

const getProducts = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    let cate_id = req.query.cate_id;
    db.productModel.find({userid:loginuser,cate_id:cate_id}).then(async (product:any) => {
        SuccessResponse.data = product
        SuccessResponse.message = MSuccess
        res.status(OK).json(SuccessResponse).end()
    }).catch(err => {
        SomethingWentWrong.data = err
        res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
    })
}
const getProductsByName = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const name = req.query.name;
    const cate_id = req.query.cate_id;
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    db.productModel.find({ name : { $regex: name, $options: 'i' }, userid:loginuser,cate_id:cate_id }).then(async (category:any) => {
        SuccessResponse.data = category
        SuccessResponse.message = MSuccess
        res.status(OK).json(SuccessResponse).end()
    }).catch(err => {
        SomethingWentWrong.data = err
        res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
    })
}




router.post(SADD_PRODUCT, addProduct); // validation pending
router.put(SEDIT_PRODUCT,editProduct); // validation pending
router.get(SGET_PRODUCT,getProducts);
router.get(SGET_PRODUCT_BYNAME,getProductsByName)

export default router;
