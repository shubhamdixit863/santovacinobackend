import * as db from '@models/index';
import { BadRequestResponse, SomethingWentWrong, SuccessResponse } from '@shared/constants';
import { MWorngPassword,MSuccess } from '@shared/Message';
import { JwtService } from '@shared/JwtService';
import { SADD_CATEGORY,SGET_CATEGORY,SGET_CATEGORY_BYNAME } from '@shared/Slug';
import { VProfile } from '@shared/validations';
import { NextFunction, Request, Response, Router } from 'express';
import { validationResult } from 'express-validator';
import { BAD_REQUEST, CREATED, INTERNAL_SERVER_ERROR, OK, UNAUTHORIZED, NOT_FOUND } from 'http-status-codes';
import category from '@models/category';
import { mongoose } from '@colyseus/social';




// Init shared
const router = Router() // .use(adminMW);
const jwtService = new JwtService();


/******************************************************************************
 *                       Register - "POST /api/category/addCategory"
 ******************************************************************************/

const addCategory = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    const label = req.body.label;
    const image = req.body.image;
    var newCategory = new db.categoryModel({
        label: label,
        image: image,
        userid: loginuser
    });
    newCategory.save().then(async (category: any) => {
        category = category.toObject()
        SuccessResponse.data = category
        res.status(CREATED).json(SuccessResponse).end()
    }).catch(err => {
        SomethingWentWrong.data = err
        res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
    })
}

const GetCategory = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    db.categoryModel.find({userid:loginuser}).then(async (category:any) => {
        SuccessResponse.data = category
        SuccessResponse.message = MSuccess
        res.status(OK).json(SuccessResponse).end()
    }).catch(err => {
        SomethingWentWrong.data = err
        res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
    })
}
const GetCategoryByName = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const label = req.query.label;
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    db.categoryModel.find({ label : { $regex: label, $options: 'i' }, userid:loginuser }).then(async (category:any) => {
        SuccessResponse.data = category
        SuccessResponse.message = MSuccess
        res.status(OK).json(SuccessResponse).end()
    }).catch(err => {
        SomethingWentWrong.data = err
        res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
    })
}




router.post(SADD_CATEGORY, addCategory); // validation pending
router.get(SGET_CATEGORY,GetCategory);
router.get(SGET_CATEGORY_BYNAME,GetCategoryByName)

export default router;
