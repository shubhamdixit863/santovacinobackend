import { Request, Response, Router, NextFunction } from 'express';
import { BAD_REQUEST, CREATED, OK } from 'http-status-codes';
import { SampleTestRoom } from '@shared/SampleTestRoom';
import { SCREATE } from '@shared/Slug';
import gameServer from '..';


const router = Router()

const createRoom = (req: Request, res: Response, next: NextFunction) => {
    const { roomName } = req.body;
    gameServer.define(roomName, SampleTestRoom)
    res.status(OK).json({ status: 'Success' }).end();
}


/******************************************************************************
 *                      Create Room - "POST /api/room/create"
 ******************************************************************************/
router.post(SCREATE, createRoom)

export default router;