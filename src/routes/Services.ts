import * as db from '@models/index';
import { BadRequestResponse, SomethingWentWrong, SuccessResponse } from '@shared/constants';
import { MSuccess,MNameAlreadyAdded,MServiceUpdated } from '@shared/Message';
import { JwtService } from '@shared/JwtService';
import { SADD_SERVICE, SEDIT_SERVICE, SGET_SERVICE, SGET_SERVICE_BYNAME} from '@shared/Slug';
import { VProfile } from '@shared/validations';
import { NextFunction, Request, Response, Router } from 'express';
import { validationResult } from 'express-validator';
import { BAD_REQUEST, CREATED, INTERNAL_SERVER_ERROR, OK, UNAUTHORIZED, NOT_FOUND } from 'http-status-codes';
import { mongoose } from '@colyseus/social';




// Init shared
const router = Router() // .use(adminMW);
const jwtService = new JwtService();


/******************************************************************************
 *                       Register - "POST /api/service/addService"
 ******************************************************************************/

const addService = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    const name = req.body.name;
    const image = req.body.image;
    const description = req.body.description;
    const service_type = req.body.service_type;
    const cate_id = req.body.cate_id;
    var newService = new db.serviceModel({
        name: name,
        image: image,
        description: description,
        service_type : service_type,
        cate_id : cate_id,
        userid: loginuser
    });
    newService.save().then(async (service: any) => {
        service = service.toObject()
        SuccessResponse.data = service
        res.status(CREATED).json(SuccessResponse).end()
    }).catch(err => {
        SomethingWentWrong.data = err
        res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
    })
}

const editService = async (req:Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    const loginuser = _id;
    const id  = req.body._id;
    const name = req.body.name;
    const image = req.body.image;
    const description = req.body.description;
    const service_type = req.body.service_type;
    const cate_id = req.body.cate_id;
    db.serviceModel.findOne({name:{'$regex' : name, '$options' : 'i'},_id: {'$ne':id}}).then(async (service:any) => {
    if(service){
        BadRequestResponse.message = MNameAlreadyAdded
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    } else {
      let service = {
        name:name,
        image:image,
        description:description,
        service_type:service_type,
        cate_id:cate_id,
      }
    db.productModel.updateOne({_id:id}, {$set:service},{new:true}).then(async (serviceData) => {
        SuccessResponse.data = serviceData
        SuccessResponse.message = MServiceUpdated
        res.status(OK).json(SuccessResponse).end()  
    }).catch(err => {
        SomethingWentWrong.data = err
        res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
    })  
    }
    })
}

const getServices = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    let cate_id = req.query.cate_id;
    db.serviceModel.find({userid:loginuser,cate_id:cate_id}).then(async (service:any) => {
        SuccessResponse.data = service
        SuccessResponse.message = MSuccess
        res.status(OK).json(SuccessResponse).end()
    }).catch(err => {
        SomethingWentWrong.data = err
        res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
    })
}
const getServicesByName = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const name = req.query.name;
    const cate_id = req.query.cate_id;
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    db.serviceModel.find({ name : { $regex: name, $options: 'i' }, userid:loginuser,cate_id:cate_id }).then(async (services:any) => {
        SuccessResponse.data = services
        SuccessResponse.message = MSuccess
        res.status(OK).json(SuccessResponse).end()
    }).catch(err => {
        SomethingWentWrong.data = err
        res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
    })
}




router.post(SADD_SERVICE, addService); // validation pending
router.put(SEDIT_SERVICE,editService); // validation pending
router.get(SGET_SERVICE,getServices);
router.get(SGET_SERVICE_BYNAME,getServicesByName)

export default router;
