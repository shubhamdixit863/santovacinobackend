import * as User from '@services/userService';
import { BadRequestResponse, cookieProps, SuccessResponse, SuccessResponseWithoutData, LOGIN_STATUS, UnauthorizedResponse } from '@shared/constants';
import { isPasswordValid } from '@shared/functions';
import { JwtService } from '@shared/JwtService';
import { MInvalideEmailNPassword } from '@shared/Message';
import { SLOGIN, SLOGOUT, SLOGINUSERSTATUS } from '@shared/Slug';
import { VLogin } from '@shared/validations';
import { NextFunction, Request, Response, Router } from 'express';
import { validationResult } from 'express-validator';
import { BAD_REQUEST, OK, NOT_FOUND, UNAUTHORIZED } from 'http-status-codes';
import { MlogOutSuccess, MSomethingWentWrong, MlogOutFromOther, MInvalidEmail, MSuccess } from '@shared/Message';




const router = Router();
const jwtService = new JwtService();

/******************************************************************************
 *                      Login User - "POST /api/auth/login"
 ******************************************************************************/
const Userlogin = (req: Request, res: Response, next: NextFunction) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { email, password } = req.body
    User.findUserByEmail(email).then(async (user) => {
        if (!user) {
            UnauthorizedResponse.message = MInvalideEmailNPassword
            UnauthorizedResponse.status = UNAUTHORIZED
            return res.status(UNAUTHORIZED).json(UnauthorizedResponse).end();
        }
        if (user && isPasswordValid(user, password)) {
            User.findUserByEmail(email).then(async (userData: any) => {
                userData = userData.toObject()
                SuccessResponse.data = userData
                res.status(OK).json(SuccessResponse).end()
            })
        } else {
            UnauthorizedResponse.message = MInvalideEmailNPassword
            UnauthorizedResponse.status = UNAUTHORIZED
            return res.status(UNAUTHORIZED).json(UnauthorizedResponse).end();
        }
    })
}

const userLogOut = (req: Request, res: Response, next: NextFunction) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { email } = req.body
    User.findUserByEmail(email).then(async (user: any) => {
        User.findUserByEmail(user.email).then(async (userData: any) => {
            if (userData) {
                userData = userData.toObject()
                let deviceInfo = userData.devices[0];
                await User.updateById(user._id, { devices: { id: deviceInfo.id, isLogin: LOGIN_STATUS.ACTIVE, token: deviceInfo.token, platform: deviceInfo.platform } })
                res.status(OK).json(SuccessResponseWithoutData).end()
            } else {
                BadRequestResponse.message = MInvalidEmail
                return res.status(BAD_REQUEST).json(BadRequestResponse).end();
            }

        })

    })
}

const userLoginStatus = (req: Request, res: Response, next: NextFunction) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { email } = req.body
    User.findUserByEmail(email).then(async (user: any) => {
        User.findUserByEmail(user.email).then(async (userData: any) => {
            if (userData) {
                userData = userData.toObject()
                let deviceInfo = userData.devices[0];
                SuccessResponse.data = deviceInfo
                res.status(OK).json(SuccessResponse).end()
            } else {
                BadRequestResponse.message = MInvalidEmail
                return res.status(BAD_REQUEST).json(BadRequestResponse).end();
            }

        })

    })
}

router.post(SLOGIN, VLogin, Userlogin)
router.post(SLOGINUSERSTATUS, userLoginStatus)
router.post(SLOGOUT, userLogOut)



/******************************************************************************
 *                      Logout - "GET /api/auth/logout"
 ******************************************************************************/

router.get('/logout', async (req: Request, res: Response) => {
    const { key, options } = cookieProps;
    res.clearCookie(key, options);
    return res.status(OK).end();
});


/******************************************************************************
 *                                 Export Router
 ******************************************************************************/

export default router;
