import * as db from '@models/index';
import { BadRequestResponse, SomethingWentWrong, SuccessResponse } from '@shared/constants';
import { MNoOffer,MSuccess } from '@shared/Message';
import { JwtService } from '@shared/JwtService';
import { SADD_PRODUCT_OFFER,SADD_SERVICE_OFFER,SGET_PRODUCT_OFFER,SGET_SERVICE_OFFER } from '@shared/Slug';
import { VProfile } from '@shared/validations';
import { NextFunction, Request, Response, Router } from 'express';
import { validationResult } from 'express-validator';
import { BAD_REQUEST, CREATED, INTERNAL_SERVER_ERROR, OK, UNAUTHORIZED, NOT_FOUND } from 'http-status-codes';




// Init shared
const router = Router() // .use(adminMW);
const jwtService = new JwtService();


/******************************************************************************
 *                       Register - "POST /api/category/addCategory"
 ******************************************************************************/

const addProductOffers = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    const user_id = req.body.user_id;
    const product_id = req.body.product_id;
    var newOffer = new db.offersModel({
        user_id: user_id,
        login_userId: loginuser,
        product_id :  product_id,
    });
    newOffer.save().then(async (productOffer: any) => {
        productOffer = productOffer.toObject()
        SuccessResponse.data = productOffer
        res.status(CREATED).json(SuccessResponse).end()
    }).catch(err => {
        SomethingWentWrong.data = err
        res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
    })
}
const addServiceOffers = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    const user_id = req.body.user_id;
    const service_id = req.body.service_id;
    var newOffer = new db.offersModel({
        user_id: user_id,
        login_userId: loginuser,
        service_id :  service_id,
    });
    newOffer.save().then(async (serviceOffer: any) => {
        serviceOffer = serviceOffer.toObject()
        SuccessResponse.data = serviceOffer
        res.status(CREATED).json(SuccessResponse).end()
    }).catch(err => {
        SomethingWentWrong.data = err
        res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
    })
}

const GetProductOffers = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    db.offersModel.find({user_id:loginuser},{service_id:0}).populate('product_id', 'userid name image description cate_id status').then(async (productOffer:any) => {
        if (productOffer.length != 0) {
            let resultSet = productOffer.map(function (offer: any) {
                let obj = {
                    _id: '',
                    userid: '',
                    name: '',
                    image: '',
                    description: '',
                    cate_id: '',
                    status: ''
                };
                if (offer.product_id) {
                    obj._id = offer._id;
                    obj.userid = offer.product_id.userid;
                    obj.name = offer.product_id.name;
                    obj.image = offer.product_id.image;
                    obj.description = offer.product_id.description;
                    obj.cate_id = offer.product_id.cate_id;
                    obj.status = offer.product_id.status;
                    return obj;
                }
            });
            Promise.all(resultSet)
                .then(function () {
                    SuccessResponse.data = resultSet
                    SuccessResponse.message = MSuccess
                    res.status(OK).json(SuccessResponse).end()
                });
        } else {
            BadRequestResponse.message = MNoOffer
            return res.status(NOT_FOUND).json(BadRequestResponse).end();
        }
    }).catch(err => {
        SomethingWentWrong.data = err
        res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
    })
}

const GetServiceOffers = async (req: Request, res: Response) => {
    const errors = validationResult(req); // validation
    if (!errors.isEmpty()) {
        const error = errors.array();
        BadRequestResponse.message = error[0].msg;
        return res.status(BAD_REQUEST).json(BadRequestResponse).end();
    }
    const { _id } = await jwtService.decodeJwt(req.headers.authorization || '');
    let loginuser = _id;
    db.offersModel.find({user_id:loginuser},{product_id:0}).populate('service_id', 'userid name image description cate_id status').then(async (serviceOffer:any) => {
        if (serviceOffer.length != 0) {
            let resultSet = serviceOffer.map(function (offer: any) {
                let obj = {
                    _id: '',
                    userid: '',
                    name: '',
                    image: '',
                    description: '',
                    cate_id: '',
                    status: ''
                };
                if (offer.service_id) {
                    obj._id = offer._id;
                    obj.userid = offer.service_id.userid;
                    obj.name = offer.service_id.name;
                    obj.image = offer.service_id.image;
                    obj.description = offer.service_id.description;
                    obj.cate_id = offer.service_id.cate_id;
                    obj.status = offer.service_id.status;
                    return obj;
                }
            });
            Promise.all(resultSet)
                .then(function () {
                    SuccessResponse.data = resultSet
                    SuccessResponse.message = MSuccess
                    res.status(OK).json(SuccessResponse).end()
                });
        } else {
            BadRequestResponse.message = MNoOffer
            return res.status(NOT_FOUND).json(BadRequestResponse).end();
        }
    }).catch(err => {
        SomethingWentWrong.data = err
        res.status(INTERNAL_SERVER_ERROR).json(SomethingWentWrong).end()
    })
}




router.post(SADD_PRODUCT_OFFER, addProductOffers); // validation pending
router.post(SADD_SERVICE_OFFER, addServiceOffers); // validation pending
router.get(SGET_PRODUCT_OFFER,GetProductOffers);
router.get(SGET_SERVICE_OFFER,GetServiceOffers);


export default router;
