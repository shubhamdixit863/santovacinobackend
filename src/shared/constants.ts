import { BAD_REQUEST,NOT_FOUND, INTERNAL_SERVER_ERROR, OK, UNAUTHORIZED } from 'http-status-codes';
import { MSomethingWentWrong, MSuccess, MUnauth,MFail,MSucc,MlogOutSuccess, MVaidationError, MWorngPassword, MPasswordNotMatched} from './Message';

// Strings
export const paramMissingError = 'One or more of the required parameters was missing.';
export const loginFailedErr = 'Login failed';

// Numbers
export const pwdSaltRounds = 12;

// Cookie Properties
export const cookieProps = Object.freeze({
    key: 'token',
    secret: process.env.COOKIE_SECRET,
    options: {
        httpOnly: true,
        signed: true,
        path: (process.env.COOKIE_PATH),
        maxAge: Number(process.env.COOKIE_EXP),
        domain: (process.env.COOKIE_DOMAIN),
        secure: (process.env.SECURE_COOKIE === 'true'),
    },
});

export const Platform = {
    ios: 'ios',
    android: 'android',
    windows: 'windows'
}

export const Language = {
    english: 'en'
}

export const Status = {
    active: 1,
    inactive: 0
}

export const Gender = {
    male: 1,
    female: 2,
    coupleMale: 3,
    coupleFemale: 4,
    transMale: 5,
    transFemale: 6
}

export const SuccessResponse = {
    status: OK,
    message: MSuccess,
    success:MSucc,
    profileUrl:"",
    data: {}
}
export const SuccessResponseWithoutData = {
    status: OK,
    message: MlogOutSuccess,
    success:MSucc,
}


export const BadRequestResponse = {
    status: BAD_REQUEST,
    message: MVaidationError,
    success:MFail,
    data: {}
};
export const NotFoundResponse = {
    status: NOT_FOUND,
    message: MVaidationError,
    success:MFail,
    data: {}
};

export const SomethingWentWrong = {
    status: INTERNAL_SERVER_ERROR,
    message: MSomethingWentWrong,
    success:MFail,
    data: {}
}

export const UnauthorizedResponse = {
    status: UNAUTHORIZED,
    message: MUnauth,
    success:MFail,
    data: {}
}

export const ROLES = {
    ADMIN: 1,
    USER: 2,
}
export const PERMISSIONS = {
    VIP: "all_poses,undress",
    FREE_USER:"",
    ADMIN: '',
    STANDARD:''
}
export const REGISTERTYPE = {
    SELFREGISTER : 1,
    FACEBOOKREGI : 2,
    GMAILREGI    : 3
}

export const LOGIN_STATUS = {
    ACTIVE: 1,
    INACTIVE: 0
}  

export const passwordUpdate = {
    status: BAD_REQUEST,
    message:MWorngPassword,      
    success:MFail,
    data: {}
}

export const match_not_password = {
    status: BAD_REQUEST,
    message:MPasswordNotMatched,
    success:MFail,
    data: {}    
}


  








export const KEY_DBCREDENTIALS = 'dbcredentials';
export const KEY_USER = 'user';