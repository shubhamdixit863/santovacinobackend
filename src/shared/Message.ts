export const MSuccess = 'Success.'
export const MFail = 'false'
export const MSucc = 'true'
export const MVaidationError = 'Validation error.'
export const MRoomName = 'RoomName is Required.'
export const MEmailExists = 'Email ID already exist.'
export const MInvalidEmail = 'Email ID is not valid.'
export const MEmailRequired = 'Email ID is Required.'
export const MUserNameExits = 'User Name already exists.'
export const MUserNameRequired = 'User Name Required.'
export const MPasswordRequired = 'Password is Required.'
export const MGenderRequired = 'Gender is Required.'
export const MDobRequired = 'Date of birth is Required.'
export const MPasswordTooShort = 'Password is too short.'
export const MSomethingWentWrong = 'Something went wrong.'
export const MInvalideEmailNPassword = 'Invalid Email or Password.'
export const MUnauth = 'User is not authorized.';
export const MUserIdRequired = 'User id is Required.'
export const MInvalidUserId = 'User id is not valid.'
export const MAlreadyExistInFriendList = 'Request already sent.';
export const MUserNotExist = 'User not exist';
export const MNoFriendsExist = 'No record found.'
export const MWorngPassword = 'wrong Password.'
export const MPasswordNotMatched = 'Password not matched'
export const MlogOutFromOther = 'Please logout from other device'
export const MlogOutSuccess = 'logout successfully'
export const MAgeBelow18 = 'You must be 18 years old or above'
export const MINVALIDTOKEN = 'Invalid Token'
export const MPROFILEUPLOAD = 'Profile Uploaded Successfully'
export const MProfileRequired = 'Profile Picture is Required.'
export const MNameAlreadyAdded = 'This name already added'
export const MProductSUpdated = 'Product Updated Successfully'
export const MServiceUpdated = 'Service Updated Successfully'
export const MNoOffer = 'No Offers'












