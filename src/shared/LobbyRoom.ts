import { MapSchema, Schema, type } from '@colyseus/schema';
import * as User from '@services/userService';
import { Client, Room } from 'colyseus';
import { wsUserVerify } from '@routes/middleware';
import logger from './Logger';

class Player extends Schema {
    @type('string') userId: string | undefined;
    @type('string') sessionId: string | undefined;
    @type('number') connectAt: number | undefined;
    @type('boolean') connected: boolean | undefined;
}


// tslint:disable-next-line: max-classes-per-file
class LobbyState extends Schema {
    @type({ map: Player }) players = new MapSchema<Player>();
}

// tslint:disable-next-line: max-classes-per-file
export class CustomLobbyRoom extends Room {


    async onCreate(options: any) {
        // await super.onCreate(options);
        this.setState(new LobbyState());
    }


    async onAuth(client: Client, options: any) {
        const tokenVerify = await wsUserVerify(options.token)
        if (tokenVerify) {
            return await User.findUserById(tokenVerify._id) || false
        } else {
            return false
        }
    }


    onJoin(client: Client, options: any, auth: any) {
        if (auth) {
            const player = new Player()
            player.userId = String(auth._id)
            player.sessionId = client.sessionId
            player.connectAt = Date.now()
            player.connected = true
            this.state.players[client.sessionId] = player
        }
    }


    async onLeave(client: Client, consented: boolean) {
        // super.onLeave(client);
        this.state.players[client.sessionId].connected = false;

        try {
            if (consented) {
                throw new Error('consented leave!');
            }
            const reConnect = await this.allowReconnection(client, 10);
            logger.debug('reConnect' + JSON.stringify(reConnect))
        } catch (e) {
            delete this.state.entities[client.sessionId];
        }
    }
}