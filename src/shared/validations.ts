import * as User from '@services/userService';
import { ROOM_NAME } from '@shared/constantsKeys';
import { body, check } from 'express-validator';
import {
    MEmailExists,
    MEmailRequired,
    MRoomName,
    MInvalidEmail,
    MPasswordRequired,
    MPasswordTooShort,
    MUserIdRequired,
    MInvalidUserId,
    MGenderRequired,
    MDobRequired,
    MUserNameExits,
    MUserNameRequired,
    MProfileRequired
} from './Message';
import { Gender, Platform } from './constants';


export const VCreateRoom = [
    check(ROOM_NAME).not().isEmpty().withMessage(MRoomName).trim().escape(),
]

export const VRegisterUser = [
    check('first_name').notEmpty().withMessage(MUserNameRequired).trim().escape(),
    body('first_name').custom(async value => {
        let status = true
        await User.findByDisplayName(value).then((user: any) => {
            if (user) {
                status = false;
                throw new Error(MUserNameExits)

            }
        })
    }),
    check('last_name').notEmpty().withMessage(MUserNameRequired).trim().escape(),
    body('last_name').custom(async value => {
        let status = true
        await User.findByDisplayName(value).then((user: any) => {
            if (user) {
                status = false;
                throw new Error(MUserNameExits)

            }
        })
    }),
    check('email').notEmpty().withMessage(MEmailRequired).isEmail().withMessage(MInvalidEmail).trim(),
    check('email').notEmpty().withMessage(MEmailRequired).trim().escape(),
    check('email').isEmail().trim(),
    body('email').custom(async value => {
        let status = true
        await User.findUserByEmail(value).then((user: any) => {
            if (user) {
                status = false;
                throw new Error(MEmailExists);
            }
        });
        return status
    }),
]

export const VLogin = [
    check('email').notEmpty().withMessage(MEmailRequired).isEmail().withMessage(MInvalidEmail).trim(),
    check('password').notEmpty().withMessage(MPasswordRequired).isLength({ min: 6 }).withMessage(MPasswordTooShort).trim(),
]

export const VSendFriendRequest = [
    check('user_id').notEmpty().withMessage(MUserIdRequired).trim(),
    body('user_id').custom(async value => {
        let status = true
        await User.findUserById(value).then((user: any) => {
            if (!user) {
                status = false;
                throw new Error(MInvalidUserId);
            }
        });
        return status
    })
]
export const VAcceptFriendRequest = [
    check('user_id').notEmpty().withMessage(MUserIdRequired).trim(),
    body('user_id').custom(async value => {
        let status = true
        await User.findUserById(value).then((user: any) => {
            if (!user) {
                status = false;
                throw new Error(MInvalidUserId);
            }
        });
        return status
    })
]
export const VProfile = [
    check('profile_picture').notEmpty().withMessage(MProfileRequired).trim(),
]
