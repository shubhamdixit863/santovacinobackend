/*************************************Users Apis***********************************************/
export const SROOM = '/room'
export const SCREATE = '/create'
export const SREGISTER = '/register'
export const SSOCIALREGISTER = '/social_register'
export const SLOGIN = '/login'
export const SLOGOUT = '/logout'
export const SLOGINUSERSTATUS = '/loginUserStatus'
export const SUSERS = '/users'
export const SSETPASSWORD = '/setPassword'
export const SCHANGEPASSWORD = '/changePassword'
export const SEDITPROFILE = '/editProfile'
export const SPROFILE_DETAILS = '/UserProfileDetails'
export const SMUMBERDETAILS = '/userMumberDetails'
export const SAUTH = '/auth'


/*************************************Assets Apis***********************************************/
export const SASSETS = '/assets'
export const SGET_ASSETS = '/getAssetsList';
export const SFILE = '/saveFile';



/*************************************Friends Management Apis***********************************************/
export const SFRIEND = '/friend'
export const SSEND_FRIEND_REQUEST = '/sendFriendRequest';
export const SGET_FRIENDS_LIST = '/getFriendsList';
export const SACCEPT_FRIEND_REQUEST = '/acceptFriendRequest';
export const SREMOVE_FRIEND = '/removeFriend';
export const SBLOCK_FRIEND = '/blockFriend';
export const SUNBLOCK_FRIEND = '/unBlockFriend';
export const SPENDING_FRIENDS_LIST = '/getPendingFriendsRequestList';
export const SBLOCKED_FRIENDS_LIST = '/getBlockedFriendsList';


/*************************************Category Apis***********************************************/
export const SCATEGORY = '/category';
export const SADD_CATEGORY = '/addCategory';
export const SGET_CATEGORY = '/getCategory';
export const SGET_CATEGORY_BYNAME = '/searchCategory';


/*************************************Product Apis***********************************************/
export const SPRODUCT = '/product';
export const SADD_PRODUCT = '/addProduct';
export const SGET_PRODUCT = '/getProducts';
export const SGET_PRODUCT_BYNAME = '/searchProducts';
export const SEDIT_PRODUCT = '/editProduct';



/*************************************Services Apis***********************************************/
export const SSERVICE = '/service';
export const SADD_SERVICE = '/addService';
export const SGET_SERVICE = '/getServices';
export const SGET_SERVICE_BYNAME = '/searchServices';
export const SEDIT_SERVICE = '/editService';




/*************************************Offers Apis***********************************************/
export const SOFFER = '/offer';
export const SADD_PRODUCT_OFFER = '/addProductOffer';
export const SADD_SERVICE_OFFER = '/addServiceOffer';
export const SGET_PRODUCT_OFFER = '/getProductOffer';
export const SGET_SERVICE_OFFER = '/getServiceOffer';










