import { MapSchema, Schema, type } from '@colyseus/schema';
import { IUser } from '@colyseus/social';
import { wsUserVerify } from '@routes/middleware';
import * as User from '@services/userService';
import { Client, generateId, Room } from 'colyseus';


class Entity extends Schema {
    @type('number')
    x: number = 0;

    @type('number')
    y: number = 0;
}

// tslint:disable-next-line: max-classes-per-file
class Player extends Entity {
    @type('boolean')
    connected: boolean = true;
}

// tslint:disable-next-line: max-classes-per-file
class Enemy extends Entity {
    @type('number')
    power: number = Math.random() * 10;
}

// tslint:disable-next-line: max-classes-per-file
class State extends Schema {
    @type({ map: Entity })
    entities = new MapSchema<Entity>();
}


// tslint:disable-next-line: max-classes-per-file
class Message extends Schema {
    @type('number') num?: number;
    @type('string') str?: string;
}

// tslint:disable-next-line: max-classes-per-file
export class SampleTestRoom extends Room {
    onCreate(options: any) {
        // console.log('DemoRoom created!', options);

        this.setState(new State());
        this.populateEnemies();

        this.setMetadata({
            str: 'hello',
            number: 10
        });

        this.setPatchRate(1000 / 20);
        this.setSimulationInterval((dt) => this.update(dt));

        this.onMessage(0, (client, message) => {
            client.send(0, message);
        });

        this.onMessage('schema', (client) => {
            const message = new Message();
            message.num = Math.floor(Math.random() * 100);
            message.str = 'sending to a single client';
            client.send(message);
        })

        this.onMessage('move_right', (client) => {
            this.state.entities[client.sessionId].x += 0.01;

            this.broadcast('hello', { hello: 'hello world' });
        });

        this.onMessage('*', (client, type, message) => { 
            console.log(`received message "${type}" from ${client.sessionId}:`, message);
        });
    }


    async onAuth(client: any, options: any) {
        const tokenVerify = await wsUserVerify(options.token)
        if (tokenVerify) {
            return await User.findUserById(tokenVerify._id) || false
        } else {
            return false
        }
    }


    populateEnemies() {
        for (let i = 0; i <= 3; i++) {
            const enemy = new Enemy();
            enemy.x = Math.random() * 2;
            enemy.y = Math.random() * 2;
            this.state.entities[generateId()] = enemy;
        }
    }


    onJoin(client: Client, options: any, user: IUser) {
        console.log('client joined!', client.sessionId);
        this.state.entities[client.sessionId] = new Player();

        client.send('type', { hello: true });
    }


    async onLeave(client: Client, consented: boolean) {
        this.state.entities[client.sessionId].connected = false;

        try {
            if (consented) {
                throw new Error('consented leave!');
            }

            console.log('let\'s wait for reconnection!')
            const newClient = await this.allowReconnection(client, 10);
            console.log('reconnected!', newClient.sessionId);

        } catch (e) {
            console.log('disconnected!', client.sessionId);
            delete this.state.entities[client.sessionId];
        }
    }


    update(dt?: number) {
        // console.log("num clients:", Object.keys(this.clients).length);
    }


    onDispose() {
        console.log('disposing DemoRoom...');
    }

}


// // tslint:disable-next-line: max-classes-per-file
// export class SampleTestRoom extends Room {

//     maxClients = 4;

//     autoDispose: boolean = true

//     userId: string | undefined | number;


//     constructor() {
//         super()
//     }


//     // When room is initialized
//     onCreate(options: any) {
//         const mesg = new GameRoomMessageSchema();
//         mesg.data = 'Auto replied, HI';
//         mesg.date = String(Date.now);
//         this.onMessage('message', (client, message) => {
//             client.send('message', mesg);
//         });
//     }


//     // Authorize client based on provided options before WebSocket handshake is complete
//     async onAuth(client: Client, options: any, request: http.IncomingMessage) {
//         const token = verifyToken(options.token);
//         return token;
//     }


//     // When client successfully join the room
//     onJoin(client: Client, options: any, auth: any) {
//         const mesg = new GameRoomMessageSchema();
//         mesg.data = 'Auto replied, HI';
//         mesg.date = String(Date.now());
//         client.send(mesg)
//         if (this.hasReachedMaxClients()) {
//             this.broadcast('message', { 'message': 'Max 4 clien can be get connect' })
//         } else {
//             client.send('message', mesg);
//             // client.send('message', { 'message': 'onJoin hello' });
//         }
//     }


//     // When a client leaves the room
//     onLeave(client: Client, consented: boolean) {
//         client.send('message', { 'message': 'onLeave hello' });
//     }


//     // Cleanup callback, called after there are no more clients in the room. (see `autoDispose`)
//     async onDispose() {
//         // await removeRoomFromDatabase();
//         logger.info('Dispose ChatRoom');
//     }
// }