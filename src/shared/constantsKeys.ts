export const KEY_DBCREDENTIALS = 'dbcredentials';
export const KEY_USER = 'user';
export const KEY_DATABASE = 'database';
export const KEY_LABEL = 'label';
export const KEY_PASSWORD = 'password';
export const KEY_CONFIRM_PASSWORD = 'confirm_password';
export const KEY_ADDRESS = 'address';
export const KEY_BEFORE_IP_ADDRESS = 'before_ip'
export const KEY_PORT = 'port';
     

export const ROOM_NAME = 'roomName'