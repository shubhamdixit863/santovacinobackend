import { Schema, type } from '@colyseus/schema';


export class GameRoomMessageSchema extends Schema {
    @type('string') data: string | undefined;
    @type('string') date: string | undefined;
}