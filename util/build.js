const fs = require('fs-extra');
const childProcess = require('child_process');


try {
    // Remove current build
    fs.removeSync('./dist/');
    // Copy front-end files
    fs.copySync('./src/public', './dist/public');
    //fs.copySync('./src/models', './dist/models');   
    //fs.copySync('./src/config', './dist/config');
    fs.copySync('./src/views', './dist/views');
    // Transpile the typescript files
    childProcess.exec('tsc --build tsconfig.prod.json', (err, stdout, stdrr) => {
        if (err) console.log(err)
        console.log(stdout)
        console.log(stdrr)
    });
} catch (err) {
    console.log(err);
}
